from .annonce import *
from .Agence import *
from .rendez_vous import *
import os
import time
import re
import sys

class Main:

    def __init__(self):        
        self.agence = Agence()
        self.menu()

    def cls(self):
            os.system('cls' if os.name=='nt' else 'clear')
    
    def menu(self):
        self.cls()
        print('############################')
        print('########### MENU ##########')
        print('############################')
        print()
        print()
        print('Restaurer une agence          -> 1')
        print('Enregistrer un client         -> 2')
        print('Enregistrer un bien           -> 3')
        print('Afficher clients              -> 4')
        print('Afficher biens                -> 5')
        print('enregistrer un RDV            -> 6')
        print('Afficher rendez-vous          -> 7')
        print('enregistrer une Annonce       -> 8')
        print('Afficher Annonce              -> 9')
        print("Supprimer TOUS les clients    -> 10")
        print("Supprimer TOUS les Rendez-vous-> 11")
        print("Supprimer TOUS les biens      -> 12")
        print("Supprimer TOUTES les Annonces -> 13")
        print("Supprimer UN client           -> 14")
        print("Supprimer UN Rendez-vous      -> 15")
        print("Supprimer UN biens            -> 16")
        print("Supprimer UNE Annonce         -> 17")
        print('sauvergarder une agence       -> 18')
        print('EXIT                          -> 19')
        print()
        res = input('Entrer votre choix : ')
        if res == '19':
            sortie = input('êtes-vous sur de vouloir quitter ? \n 1-oui \n 2-non\n')
            if sortie == '1':
                self.cls()
                print("Vous quittez le programme...")
                time.sleep(2)
                sys.exit()
        if res == '1': 
            fichier = str(input("Entrer le nom du fichier de sauvegarde : "))
            self.agence = self.agence.restaurer(fichier)
            self.cls()
            print("Agence restaurer ! ")
            time.sleep(2)
        if res == '2':
            self.cls()
            self.enregistrerClient()
        if res == '3':
            self.cls()
            self.enregistrerBien()
            
        if res == '4':
            self.cls()
            self.agence.afficher_client()
            suivant = input('Appuyer pour sortir.')
        if res == '5':
            self.cls()
            self.agence.afficher_bien()
            suivant = input('Appuyer pour sortir.')
        if res == '6':
            self.cls()
            self.enregistrerRDV()
        if res == '7':
            self.cls()
            self.agence.afficher_rdv()
            suivant = input('Appuyer pour sortir.')
        if res == '8':
            self.cls()
            self.enregistrerAnnonce()
        if res == '9':
            self.cls()
            self.agence.afficher_annonce()
            suivant = input('Appuyer pour sortir.')
        if res == '10':
            self.cls()
            supp = int(input("Etes-vous sur de vouloir supprimer tous les clients ? \n1-Oui \n 2-Non"))
            if supp == 1:
                self.agence.delete_all_Client()
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)

        if res == '11':
            self.cls()
            supp = int(input("Etes-vous sur de vouloir supprimer tous les rendez-vous ? \n1-Oui \n 2-Non"))
            if supp == 1:
                self.agence.delete_all_rdv()
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)

        if res == '12':
            self.cls()
            supp = int(input("Etes-vous sur de vouloir supprimer tous les biens ? \n1-Oui \n 2-Non"))

            if supp == 1:
                self.agence.delete_all_bien()
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)
        
        if res == '13':
            self.cls()
            supp = int(input("Etes-vous sur de vouloir supprimer toutes les annonces ? \n1-Oui \n 2-Non"))

            if supp == 1:
                self.agence.delete_all_annonce()
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)

        if res == '14':
            self.cls()
            identifiant = self.choisir_client()
            supp = int(input("Etes-vous sur de vouloir supprimer ce client ? \n1-Oui \n 2-Non"))

            if supp == 1:
                self.agence.delete_client(identifiant)
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)

        if res == '15':
            self.cls()
            identifiant = self.choisir_rdv()
            supp = int(input("Etes-vous sur de vouloir supprimer ce rendez-vous ? \n1-Oui \n 2-Non"))

            if supp == 1:
                self.agence.delete_rdv()
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)

        if res == '16':
            self.cls()
            identifiant = self.choisir_bien()
            supp = int(input("Etes-vous sur de vouloir supprimer ce bien ? \n1-Oui \n 2-Non"))
            if supp == 1:
                self.agence.delete_bien(identifiant)
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)

        if res == '17':
            self.cls()
            identifiant = self.choisir_annonce()
            supp = int(input("Etes-vous sur de vouloir supprimer cette annonce ? \n1-Oui \n 2-Non"))

            if supp == 1:
                self.agence.delete_annonce()
                self.cls()
                print("Suppresion Réussie !")
                time.sleep(3)
            else:
                print("Suppresion annulée, retour vers le menu")
                time.sleep(3)

        if res == '18':
            nom = str(input('Nom de votre fichier : '))
            self.agence.sauver(nom)
            self.cls()
            print(f'Agence sauvegarder dans le fichier {nom}.yaml !')
            time.sleep(3)

        #On réouvre le menu
        self.menu()
        self.cls()   

    ########## verification des saisies #################
    # Ces fonctions permettent de nous aider pour les saisies de l'utilisateur


    def saisieAnnee(self):
        year = int(input('Entrer la date : \n Annee : '))
        while not year > 2020:
            year = int(input('Erreur: Entrer une annee valide : '))
        return year

    def saisieMois(self):
        month = int(input(' Mois : '))
        while not 0 <= month <= 12:
            month = int(input('Erreur: Entrer un mois valide : '))
        return month
    
    def saisieJour(self):
        day = int(input(' Jour : '))
        while not 0 < day <= 31:
            day = int(input('Erreur: Entrer un  jour valide : '))
        return day

    def saisieHour(self):
        hour = int(input(' Heure : '))
        while not 0 <= hour <= 23:
            hour = int(input('Erreur: Entrer une heure valide : '))
        return hour

    def saisieMinute(self):
        minute = int(input(' Minute : '))
        while not 0 <= minute <= 59:
            minute = int(input('Erreur: Entrer une minute valide : '))
        return minute

    def saisieDate(self, testInput):
        regex = re.compile(r'^[0-9]{2}/[0-9]{2}/[0-9]{4}$')
        date = ""
        while not re.findall(regex, date):
            date = str(input(testInput))
        return date

    def choisir_rdv(self):
        """
            Choisir un client parmi la liste
        """
        self.agence.afficher_rdv()
        rdv = int(input("Entrer id du client : "))
        while rdv not in self.agence.rdv:
            rdv = int(input("Entrer id du client : "))
        return rdv

    def choisir_client(self):
        """
            Choisir un client parmi la liste
        """
        self.agence.afficher_client()
        client = int(input("Entrer id du client : "))
        while client not in self.agence.client:
            client = int(input("Entrer id du client : "))
        return client

    def choisir_bien(self):
        """
            Choisir un bien parmi la liste
        """
        self.agence.afficher_bien()
        bien = int(input("Entrer id du bien : "))
        while bien not in self.agence.bienImmo or bien == "exit":
            bien = int(input("Id non valide : Entrer id du bien : "))
        return bien
    
    def choisir_annonce(self):
        """
            Choisir une annonce parmi la liste
        """
        self.agence.afficher_annonce()
        annonce = int(input("Entrer id de l'annonce : "))
        while annonce not in self.agence.annonce or annonce == "exit":
            bien = int(input("Id non valide : Entrer id de l'annonce : "))
        return bien



    ##### enregistrer un rdv #######

    def enregistrerRDV(self):
        """
            Enregistrer un rendez-vous avec plusieurs options :
            -Rdv entre vendeur et agence (ex : mandat)
            -Rdv entre acheteur et agence (ex : visite))
            -Rdv entre vendeur, acheteur et agence (ex : vente)
        """
        # On propose les 3 options de saisie de RDV
        typeRdv = int(input('Faites votre choix : \n\n 1-Rdv vendeur (mandat) \n 2-Rdv acheteur (visite) \n 3-Rdv acheteur et vendeur (vente) \n 4- exit \n'))
        
        if typeRdv == 4:
            self.cls()
            self.menu()
        else:

            #l'utilisateur enregistre son rendez-vous
            self.cls()
            self.agence.afficher_bien()

            #On récupère le bien
            idBien = int(input("\nEntrer l\'id du bien : "))


            annee = self.saisieAnnee()
            mois = self.saisieMois()
            jour = self.saisieJour()
            heure = self.saisieHour()
            minute = self.saisieMinute()
            description = str(input('Description (si nécessaire) : \n '))

            rdv = RendezVous(annee, mois, jour, heure, minute, description, idBien)
            self.agence.add_rdv(idBien, rdv)

            #On demande l'ajout d'un (de) client(s) aux rdv

            if typeRdv == 1:
                #Mandat
                print("Choisir un Vendeur dans la liste : \n")
                client = self.choisir_client()
                rdv.rdv_mandat(client)

            if typeRdv == 2:
                #Visite
                print("Choisir un potentiel Acheteur dans la liste : \n")
                client = self.choisir_client()
                rdv.rdv_visite(client)

            if typeRdv == 3:
                #Vente
                print("Choisir un Vendeur dans la liste : \n")
                vendeur = self.choisir_client()
                print("Choisir un second client (acheteur) dans la liste : \n")
                acheteur = self.choisir_client()
                rdv.rdv_vente(acheteur, vendeur)
        



    ###### Enregistre un client #######

    def enregistrerClient(self):
        """
            Enregistrer un client 
        """
        typeClient = int(input("Votre client est : \n 1-Personne Physique \n 2-Personne Morale \n 3-exit"))

        if typeClient == 3:
            self.menu()
        # On entre les infos communes au deux types de Personne
        nom = str(input('Entrer le nom de votre client : '))
        adress = str(input('Entrer l\'adresse de votre client : '))
        tel = int(input('Entrer le numéro de téléphone de votre client : '))
        email = str(input('Entrer l\'email de votre client : '))
        
        #On entre les infos spécifiques poour chaque type de Personne
        if typeClient == 1:
            prenom = str(input('Entrer le prenom du client : '))

            client = Physique(nom,adress, tel, email, prenom)
            
        if typeClient == 2:
            SIREN = str(input("Entrer unméro de SIREN de l'entreprise : "))
            formeJuridique = str(input("Type d'entreprise (SAS, SARL ...) : "))

            client = Morale(nom, adress, tel, email, SIREN, formeJuridique)

        #On ajoute le client au registre des clients de l'agence    
        self.agence.add_client(client)
        return client


    ###### Enregistre un bien #######


    def enregistrerBien(self):
        """
            enregistre un bien. Lie un propriétaire (liste déjà enregistré ou ajouter un nouveau)
        """
        typeBien = int(input("Type du bien : \n 1-Maison \n 2-Appartement \n 3-Terrain \n 4-exit"))
        
        if typeBien == 4:
            self.menu()
        # On entre les infos communes au differents type de biens
        adress = str(input("Entrer l'adresse du bien : "))
        prix = int(input("Entrer le prix souhaité du bien : "))
        dateVente = self.saisieDate("Entrer la date de mise en vente du bien (jj/mm/aaaa): ")
        dateDispo = self.saisieDate("Entrer la date de disponibilité du bien (jj/mm/aaaa): ")
        orientaion = str(input("Entrer l'orientation du bien : "))

        if typeBien == 1:
            surface = float(input("Entrer la surface de votre bien : "))
            nbpiece = int(input("Entrer le nombre de piece : "))
            nbEtage = int(input("Entrer le nombre d'etage de la maison : "))
            chauffage = str(input("Quel est le chauffage de la maison ?"))
            
            #Création de maison avec les infos entrées
            bien = Maison(adress, prix, dateVente, dateDispo, orientaion, "vendeur", "acheteur", surface, nbpiece, nbEtage, chauffage)
            
        elif typeBien == 2:
            nbpiece = int(input("Entrer le nombre de piece : "))
            numEtage = int(input("Entrer le numéro d'etage : "))
            charge = int(input("Entrer les charges par mois : "))

            #Création d'appartement avec les infos entrées
            bien = Appartement(adress, prix, dateVente, dateDispo, orientaion, "vendeur", "acheteur", nbpiece, numEtage, charge)

        elif typeBien == 3:
            surface = float(input("Entrer la surface de votre bien : "))
            longueur = int(input("Entrer la longueur de votre bien : "))

            #Création de terrain avec les infos entrées
            bien = Terrain(adress, prix, dateVente, dateDispo, orientaion, "vendeur", "acheteur", surface, longueur)

        #On ajoute le bien au registre des bien de l'agence
        print("\n\nEntrer un proprio : \n")
        proprio = self.choisir_client()
        bien.add_vendeur(proprio)
        self.agence.add_bien(bien)

    
    ###### Enregistre une Annonce #######

    def enregistrerAnnonce(self):
        """
            Enregistrer une annonce avec plusieurs choix :
            -Annonce pour le web
            -Annonce pour un journal 
            -Annonce pour un magasine
        """
        bien = int(input("1-Choisir un bien dans la liste \n 2-exit\n"))
        
        if bien == 2:
            self.menu()
        else:
            self.cls()
            self.agence.afficher_bien()

            #On récupère le bien
            idBien = int(input("\nEntrer l\'id du bien : "))

            # On propose les 3 options de saisie d'annonce
            typeAnnonce = 0
            while 1 <= typeAnnonce <= 3:
                typeAnnonce = int(input('Faites votre choix : \n 1-Web \n 2-Journal \n 3-Magasine \n'))
                if 3 <= typeAnnonce and typeAnnonce <= 1:
                    print("Selectionner un type d'annonce valide !")

            description = str(input('Entrer votre description: \n '))

            annonce = Annonce(idBien, typeAnnonce, description)

        #On ajoute l'annonce au registre des annonces de l'agence
        self.agence.add_annonce(annonce)
        return annonce
