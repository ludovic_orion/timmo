from dataclasses import dataclass
import datetime
from .Personne import *


class RendezVous:
    """
        Rendez-vous décrit une date et une heure, un vendeur et/ un acheteur
    """


    def __init__(self, annee, mois, jour, heure, minute, description, idBien):
        self.date = datetime.datetime(annee, mois, jour, heure, minute)  
        self.description = description
        self.typeRDV = ""
        self.clients = []
        self.idBien = idBien
        

    def __str__(self):
        if len(self.clients) <= 1:
            c = "client"
        else:
            c = "clients"
        return f"{self.date} {c} : {self.clients}"
    
    def __repr__(self):
        if len(self.clients) <= 1:
            c = "client"
        else:
            c = "clients"
        return f"{self.date} {c} : {self.clients}"

        
    def heureRDV(self):
        """
            Retourne l'heure du rdv
        """
        return str(self.date.strftime("%X"))
    
    def jourRDV(self):
        """
            Retourne la date du rdv (mm/jj/aaaa)
        """
        return str(self.date.strftime("%x"))

    def rdv_mandat(self, vendeur):
        """
            Enregistrer un rdv pour signature mandat
            On va enregistrer un vendeur
            Retourne faux si rendez-vous déjà assigné
        """
        if self.clients != []:
            return False
        else:
            self.typeRDV = "Signature mandat"
            self.clients.append(vendeur)


    def rdv_visite(self, acheteur):
        """
            Enregistrer un rdv pour signature mandat
            On va enregistrer un acheteur
            Retourne faux si rendez-vous déjà assigné
        """
        if self.clients != []:
            return False
        else:
            self.typeRDV = "Visite"
            self.clients.append(acheteur)

    def rdv_vente(self,acheteur, vendeur):
        """
            Enregistrer un rdv pour signature mandat
            On va enregistrer un vendeur et un acheteur
            Retourne faux si rendez-vous déjà assigné
        """
        if self.clients != []:
            return False
        else:
            self.typeRDV = "Signature vente"
            self.clients.append(acheteur)
            self.clients.append(vendeur)
        
    


if __name__=='__main__':
    rdv = RendezVous(2020,1,23,3,2,"description")
    perso = Physique("Orion", 'Roquem', 675849, "ludo@mail", "Ludovic")
    perso1 = Physique("NomClient", 'Roquem', 2453545, "Client@mail", "Client")
    rdv1 = RendezVous(2021,1,23,3,2,"description")
    perso2 = Morale("NomClient", 'Roquem', 2453545, "Client@mail", 2341, "SAS")

    rdv.rdv_visite(perso1)
    rdv.rdv_mandat(perso)
    rdv1.rdv_vente(perso, perso2)
    print(rdv1)
    
