from dataclasses import dataclass
from .Personne import *


@dataclass(order=True, frozen=False)
class BienImmobilier:
    """
        Un bien est décrit par plusieurs attributs et a un propriétaire
    """

    adress : str
    prix : int
    dateVente : int
    dateDispo : int
    orientaion : str
    vendeur : Personne
    acheteur : Personne


    def str_vendeur(self):
        return self.vendeur


    def add_vendeur(self,vendeur):
        """
            Ajoute un vendeur au bien
        """
        self.vendeur = vendeur
        return self

    def add_acheteur(self,acheteur):
        """
            ajoute un acheteur au bien
        """
        self.acheteur = acheteur
        return self
  


@dataclass
class Terrain(BienImmobilier):

    """
    Terrain est une classe qui hérite de BienImmobilier
    """

    surfaceS : int
    longueurF : int

    def __hash__(self):
        return id(self)
    


@dataclass
class Maison(BienImmobilier):

    """
    Maison est une classe qui hérite de BienImmobilier
    """


    surfaceHab : int
    nbPiece : int
    nbEtage : int
    chauffage : str

    def __hash__(self):
        return id(self)
    

@dataclass
class Appartement(BienImmobilier):

    """
    Appartement est une classe qui hérite de BienImmobilier
    """

    nbPiece : int
    numEtage : int
    chargesMens : int

    def __hash__(self):
        return id(self)
    