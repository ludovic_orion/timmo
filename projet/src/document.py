class Document:
    def __init__(self, Desc):
        self.Description = Desc


class Web(Document):
    def __init__(self, Desc):
        super().__init__(Desc)
    
    def __str__(self):
        return "Création d'une page web pour l'article suivant:\n <p>"+self.Description+"<p>"


class Journal(Document):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return  "Envoie de la description suivante à un journal :\n"+self.Description

class Magasine(Document):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return  "Envoie de la description suivante à un magasine :\n"+self.Description
