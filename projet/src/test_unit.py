import unittest
from .Personne import *
from .bienimmo import *
from .rendez_vous import *
from .Agence import *


class Test(unittest.TestCase):
    """
        Test les fonctions en lien avec l'agence
    """
    perso = Physique("Orion", 'Roquem', 675849, "ludo@mail", "Ludovic")
    perso1 = Physique("NomClient", 'Roquem', 2453545, "Client@mail", "Client")
    perso2 = Morale("NomClient", 'Roquem', 2453545, "Client@mail", 2341, "SAS")
    
    bien = Appartement("Impasse Roquemaurel", 210540, 123, 123, "sud", "vendeur", "acheteur", 3, 4, 312)
    bien1 = Maison("Adresse maison", 213111, 123, 123, "sud", "vendeur", "acheteur", 123, 2, 3, "electrique")
    
    rdv = RendezVous(2020, 3, 23, 12, 34, "desciption",0)
    rdv1 = RendezVous(2021, 4, 3,2, 32, "desciption1",0)


    ####  Test BienImmobiler ####

    def test_add_vendeur(self):
        """
            Test qu'un seul vendeur ne peut etre sur un bien
        """
        self.bien.add_vendeur(self.perso)
        self.bien.add_vendeur(self.perso1)
        self.assertEqual(self.bien.vendeur, self.perso1)

    def test_add_acheteur(self):
        """
            Test qu'un seul acheteur ne peut etre sur un bien
        """
        self.bien.add_acheteur(self.perso1)
        self.bien.add_acheteur(self.perso)
        self.assertEqual(self.bien.acheteur, self.perso)


    #### Test Rendez-vous ####

    def test_heureRDV(self):
        """
            test sur l'heure
        """
        self.assertEqual(self.rdv.heureRDV(), "12:34:00")

    def test_jourRDV(self):
        """
            Test sur le jour
        """
        self.assertEqual(self.rdv.jourRDV(), "03/23/20")
    
    def test_mandat(self):
        """
            Test l'ajout d'un rdv pour signer mandat, et l'impossibilté 
            de rajouter un autre rdv par deçu celui-ci
        """

        agence = Agence()        
        rdv_mandat = RendezVous(2020, 2, 21, 2, 43, "desciption",0)
        agence.add_bien(self.bien)
        agence.add_client(self.perso)
        agence.add_rdv(0, rdv_mandat)

        agence.rdv[0].rdv_mandat(self.perso1)
        agence.rdv[0].rdv_visite(self.perso)
        self.assertEqual(agence.rdv[0].clients, [self.perso1])
        self.assertEqual(agence.rdv[0].typeRDV, "Signature mandat")

    def test_visite(self):
        """
            Test l'ajout d'un rdv pour visiter un bien, et l'impossibilté 
            de rajouter un autre rdv par deçu celui-ci
        """
        agence = Agence()
        rdv_visite = RendezVous(2021,5, 14, 11, 54, "desciption",0)
        agence.add_bien(self.bien)
        agence.add_client(self.perso)
        agence.add_rdv(0, rdv_visite)        

        agence.rdv[0].rdv_visite(self.perso1)
        agence.rdv[0].rdv_visite(self.perso)
        self.assertEqual(agence.rdv[0].clients, [self.perso1])
        
        #self.assertEqual(agence.rdv[0].typeRDV, "Visite")

    def test_vente(self):
        """
            Test l'ajout d'un rdv pour une vente, et l'impossibilté 
            de rajouter un autre rdv par deçu celui-ci
        """
        agence = Agence()
        rdv_vente = RendezVous(2020,7, 11, 22, 1, "desciption",0)
        agence.add_bien(self.bien)
        agence.add_client(self.perso)
        agence.add_rdv(0, rdv_vente)

        agence.rdv[0].rdv_vente(self.perso1, self.perso2)
        agence.rdv[0].rdv_visite(self.perso)
        self.assertEqual(agence.rdv[0].clients, [self.perso1, self.perso2])
        #self.assertEqual(agence.rdv[0].typeRDV, "Signature vente")

    #### Test Agence ####

    def test_add_client(self):
        """
            Vérifie l'ajout d'un client et l'attribution d'un nouvel ID
            On va ajouter 2 clients à l'agence
            Puis, on va vérifié que ces deux clients on tous les deux 
            un identifiant différent
        """
        agence = Agence()
        agence.add_client(self.perso)
        agence.add_client(self.perso1)
        
        self.assertEqual(agence.client[0], self.perso)
        self.assertEqual(agence.client[1], self.perso1)

    def test_add_bien(self):
        """
            Vérifie l'ajout d'un bien et l'attribution d'un nouvel ID
            On va ajouter 2 biens à l'agence
            Puis, on va vérifié que ces deux biens on chacun un identifiant différent
        """
        agence = Agence()
        agence.add_bien(self.bien)
        agence.add_bien(self.bien1)

        self.assertEqual(agence.bienImmo[0], self.bien)
        self.assertEqual(agence.bienImmo[1], self.bien1)

    def test_add_rdv(self):
        """
            On vérifie l'ajout d'un rendez-vous avec l'id d'un bien
            On test aussi la vérification de l'existance du bien
        """
        agence = Agence()
        agence.add_bien(self.bien)
        agence.add_rdv(0, self.rdv)
        agence.add_rdv(1, self.rdv)
        self.assertEqual(agence.rdv[0], self.rdv)
        self.assertNotIn(self.rdv1, agence.rdv)


    def test_delete_client(self):
        """
            Test la suppression d'un client et de ses rdvs
        """
        agence = Agence()
        rdv_test_client = RendezVous(2020, 3, 23, 12, 34, "desciption",0)
        rdv_test_client1 = RendezVous(2020, 3, 23, 12, 34, "desciption",0)

        #Le client
        agence.add_client(self.perso)

        agence.add_bien(self.bien)
        agence.add_bien(self.bien1)

        agence.add_rdv(0, rdv_test_client)
        agence.add_rdv(1, rdv_test_client1)
        agence.rdv[0].rdv_visite(self.perso)
        agence.rdv[1].rdv_visite(self.perso)

        agence.delete_client(0)
        self.assertNotIn(self.perso, agence.client)
        self.assertNotIn(rdv_test_client, agence.rdv)
        self.assertNotIn(rdv_test_client1, agence.rdv)

    def test_delete_rdv(self):
        """
            Test la suppression d'un rdv
        """
        agence = Agence()
        rdv_test_rdv = RendezVous(2020, 3, 23, 12, 34, "desciption",0)

        agence.add_bien(self.bien)
        agence.add_rdv(0, rdv_test_rdv)
        agence.delete_rdv(0)
        self.assertNotIn(rdv_test_rdv, agence.rdv)

    def test_delete_bien(self):
        """
            Test la suppression d'un bien et de ses rdv associés
        """
        agence = Agence()
        rdv_test_client = RendezVous(2020, 3, 23, 12, 34, "desciption",0)
        rdv_test_client1 = RendezVous(2020, 3, 23, 12, 34, "desciption",0)

        #Le client
        agence.add_client(self.perso)

        agence.add_bien(self.bien)

        agence.add_rdv(0, rdv_test_client)
        agence.rdv[0].rdv_visite(self.perso)

        agence.delete_bien(0)
        self.assertNotIn(self.bien, agence.bienImmo)
        self.assertNotIn(rdv_test_client, agence.rdv)
        self.assertNotIn(rdv_test_client1, agence.rdv)

    def test_delete_all_client(self):
        """
            Test la suppression des clients et de leur rdv
        """
        agence = Agence()
        rdv_test_all_client = RendezVous(2020, 3, 23, 12, 34, "desciption",0)
        agence.add_client(self.perso)
        agence.add_client(self.perso1)
        agence.add_bien(self.bien)
        agence.add_rdv(0, rdv_test_all_client)
        agence.rdv[0].rdv_visite(self.perso)
        agence.delete_all_Client()
        self.assertFalse(agence.client)
        
    def test_delete_all_bien(self):
        """
            Test la suppression des bien
        """
        agence = Agence()
        agence.add_client(self.perso)
        agence.add_client(self.perso1)
        agence.delete_all_bien()
        self.assertFalse(agence.bienImmo)

    def test_delete_all_rdv(self):
        """
            Test la suppression des rdv
        """
        agence = Agence()
        agence.add_client(self.perso)
        agence.add_client(self.perso1)
        agence.delete_all_rdv()
        self.assertFalse(agence.rdv)


    def test_sauver_restaurer(self):
        """
            Vérifie la sauvegarde puis restauration d'une agence
        """
        #Pour ce test, nous allons créer une agence que nous allons sauvegarder
        #Puis créer une nouvel agence qui va récupérer les données de la première agence
        #Si les deux agences sont égales, la sauvegarde et la restauration fonctionne

        agence = Agence()
        agence1 = agence

        agence.add_client(self.perso)
        agence.add_bien(self.bien)
        agence.add_rdv(0, self.rdv)
        agence.sauver("agence_test")

        agence1.restaurer('agence_test')
        
        #Si les 4 bibliothèques sont égales, les deux agences sont égales
        self.assertEqual(agence.client, agence1.client)
        self.assertEqual(agence.rdv, agence1.rdv)
        self.assertEqual(agence.bienImmo, agence1.bienImmo)
        self.assertEqual(agence.annonce, agence1.annonce)




    #### Test Main ####
    
    ### Des fontions ont été implémentés pour vérifiers les saisies de l'utilisateur
        


