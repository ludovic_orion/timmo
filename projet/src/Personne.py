from dataclasses import dataclass

@dataclass(order=True,frozen=False)
class Personne:
    """
      docstring
    """
    name: str
    adress: str
    tel: int
    email: str
    
    
    

@dataclass
class Physique(Personne):
    """
      Decrit une personne physique
    """

    firstname: str

    def __hash__(self):
      return id(self)

@dataclass
class Morale(Personne):
    """
        Personne morale (exemple : entreprise)
    """

    SIREN: int
    formeJuridique: str #SAS, SARL etc

    def __hash__(self):
      return id(self)