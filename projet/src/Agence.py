from dataclasses import dataclass
import yaml
import os
from .Personne import * 
from .bienimmo import *
from .rendez_vous import *
from .document import *
from .annonce import *

class Agence:
    """
        L'agence est contitué de trois bibliothèques : RDV, BienImmo, Annonce
        Chaque bibliothèque contiendra les éléments qui la concernent
    """


    def __init__(self):
        self.nom = "TIMMO"
        self.email = "mail@timmo.fr"
        self.siteWeb = "urltimmo.fr"
        

        self.bienImmo = {}
        self.id_bien = 0

        #Un client sera un acheteur ou un vendeur, physique ou morale. 
        self.client = {}
        self.id_client = 0
        
        self.rdv = {}
        self.id_rdv = 0

        self.annonce = {}           

    

    def afficher_client(self):
        """
            Propose un affichage des clients de l'agence
        """
        print("Numéro Client  Client")
        print("-------------  ------")
        for client in self.client:
            print(f"{client}                {self.client[client]}")

    def afficher_bien(self):
        """
            Propose un affichage des biens de l'agence
        """
        print("Numéro Bien Immobilier  Client")
        print("-------------  ------")
        for bien in self.bienImmo:
            print(f"{bien}                 {self.bienImmo[bien]}")

    def afficher_rdv(self):
        print('\nId Bien      Rendez-vous (aaaa-jj-mm)\n')
        print("------      ------------")
        for rdv in self.rdv:
            print(f"  {rdv}          {self.rdv[rdv]}")
    
    def afficher_rdv_bien(self, idbien):
        """
            Affiche les rendez-vous liés à un bien
        """
        print(f'\nBien n°{idbien}      Rendez-vous (aaaa-jj-mm)\n')
        print("------      ------------")
        for rdv in self.rdv:
            if self.rdv[rdv].idBien == idbien:
                print(f"  {idbien}          {self.rdv[rdv]}")

    def afficher_annonce(self):
        print('\nId Bien      Description\n')
        print("------      ------------")
        for a in self.annonce:
            print(f"  {a}          {self.annonce[a]}")


    def add_client(self, client):
        """
            Ajoute un client avec un nouvel "id  
        """
        self.client[self.id_client] = client
        self.id_client += 1
        return self.client


    def add_bien(self, elt):
        """
            Ajoute un bien avec un nouvel id
        """
        self.bienImmo[self.id_bien] = elt 
        self.id_bien += 1
        return self

    def add_rdv(self,IdBien, rdv):
        """
            Créer un nouveau rendez-vous et le lie à un bien
        """
        if IdBien in self.bienImmo:
            self.rdv[self.id_rdv] = rdv 
            self.id_rdv += 1
            return self

    def add_annonce(self, Annonce):
        """
            Créer une nouvelle annonce et la lie à un bien
        """ 
        if Annonce.IdBien in self.bienImmo:
            if Annonce.Media == 1:
                self.annonce[Annonce.IdBien ] = Web(Annonce.Description) 
            elif Annonce.Media == 2:
                self.annonce[Annonce.IdBien ] = Journal(Annonce.Description) 
            elif Annonce.Media == 3:
                self.annonce[Annonce.IdBien ] = Magasine(Annonce.Description) 

            return self.annonce[Annonce.IdBien]


    def restaurer(self, fichier):
        """
            restaurer à partit d'un fichier existant
        """
        nom = f"{fichier}.yaml"
        with open(nom, 'r') as f:
            return yaml.load(f)

     
    def sauver(self, fichier):
        """
            sauver les données de l'agence
        """
        nom = f"{fichier}.yaml"
        with open(nom, 'w') as f:
            yaml.dump(self, f)

    def delete_all_Client(self):
        """
            Supprime tous les clients de l'agence
        """
        self.client = {}
        self.id_client = 0

    def delete_all_bien(self):
        """
            Supprimer tous les biens de l'agence
        """
        for bien in self.bienImmo:
            if self.rdv[bien]:
                del(self.rdv[bien])
        self.bienImmo = {}
        self.id_bien = 0
         

    def delete_all_rdv(self):
        """
            Supprime tous les rdv de l'agence
        """
        self.rdv = {}
    
    def delete_all_annonce(self):
        """
            Supprime tous les rdv de l'agence
        """
        self.annonce = {}

    def delete_client(self, IdClient):
        """
            Supprimer un client
            On ne supprimera pas le client dans les biens
            On supprimera les rendez-vous liés à ce client
        """
        #On change les rendez-vous liés ce client
        aSupprimer = []
        for r in self.rdv:
            if self.client[IdClient] in self.rdv[r].clients:
                aSupprimer.append(r)
        for i in aSupprimer:
            del(self.rdv[i])
        del(self.client[IdClient])

    def delete_rdv(self, idRdv):
        """
            Supprimer un rdv
        """
        del(self.rdv[idRdv])

    def delete_annonce(self, idAnnonce):
        """
            Supprimer une annonce 
        """
        del(self.annonce[idAnnonce])

    def delete_bien(self, idBien):
        """
            Supprimer un bien
            On supprimera les rendez-vous liés à ce bien
        """
        #On change les rendez-vous liés ce client
        aSupprimer = []
        for r in self.rdv:
            if idBien == self.rdv[r].idBien:
                aSupprimer.append(r)
        for i in aSupprimer:
            del(self.rdv[i])
        del(self.bienImmo[idBien])
